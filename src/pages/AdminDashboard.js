import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function AdminDashboard(){

    // To validate the user role.
    const {user} = useContext(UserContext);

    // Create allProducts State to contain the courses from the database.
    const [allProducts, setAllProducts] = useState([]);

    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/menu`, {
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setAllProducts(data.map(product => {
                return (
                    <tr key={product._id}>
                        <td>{product._id}</td>
                        <td>{product.name}</td>
                        <td>{product.description}</td>
                        <td>{product.price}</td>
                        <td>{product.stocks}</td>
                        <td>{product.isActive ? "Active" : "Inactive"}</td>
                        <td>
                            {
                                (product.isActive)
                                ?
                                    <Button variant = "danger" size ="sm"  onClick = {() => archive(product._id, product.name)}>Archive</Button>
                                :
                                    <>
                                        <Button variant = "success" size ="sm" onClick = {() => unarchive(product._id, product.name)}>Unarchive</Button>
                                        <Button variant = "secondary" size = "sm">Edit</Button>
                                    </>
                            }
                        </td>
                    </tr>
                )
            }))

        })
    }

    // Making the product inactive
    const archive = (productId, productName) => {
        console.log(productId);
        console.log(productName);

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem(`token`)}`
            },
            body: JSON.stringify({
                isActive : false
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){
                Swal.fire({
                    title: "Archive Successful!",
                    icon: "success",
                    text: `${productName} is now inactive.`
                })
                fetchData();
            }
            else{
                Swal.fire({
                    title: "Archive Unsuccessful!",
                    icon: "error",
                    text: `Something went wrong. Please try again.`
                })
            }
        })
    }

    // Making the product active
    const unarchive = (productId, productName) => {
        console.log(productId);
        console.log(productName);

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem(`token`)}`
            },
            body: JSON.stringify({
                isActive : true
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){
                Swal.fire({
                    title: "Unarchive Successful!",
                    icon: "success",
                    text: `${productName} is now active.`
                })
                fetchData();
            }
            else{
                Swal.fire({
                    title: "Unarchive Unsuccessful!",
                    icon: "error",
                    text: `Something went wrong. Please try again.`
                })
            }
        })
    }

    //fetch all products
    useEffect(() => {
        fetchData();
    }, [])

    return(
        (user.isAdmin)
        ?
        <>
            <div className = "mt-5 mb-3 text-center">
                <h1>Admin Dashboard</h1>
                <Button variant = "primary" size = "lg" className = "mx-2">Add Products</Button>
                <Button variant = "success" size = "lg" className = "mx-2">Show Orders</Button>
            </div>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Product ID</th>
                        <th>Product Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Stocks</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {allProducts}
                </tbody>
            </Table>
        </>
        :
        <Navigate to = "/products/menu" />
    )
}