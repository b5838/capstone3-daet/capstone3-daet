import {useEffect, useState, useContext} from "react";
import UserContext from "../UserContext";

import {Navigate} from "react-router-dom";
import {Button, Form} from "react-bootstrap";
import Swal from "sweetalert2";

export default function Login() {

    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    // State to determine whether the button is enabled or not
    const [isActive, setIsActive] = useState(false);

    function loginUser(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            console.log(data.key)

            if(typeof data.key !== "undefined"){
                localStorage.setItem("token", data.key);
                retrieveUserDetails(data.key);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                });
            }
            else{
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Check your login details and try again"
                });
            }
        })


        // Clear input fields
        setEmail("");
        setPassword("");

    }

    // Retrieve user details
    // Get the payload from the token.
    const retrieveUserDetails = (token) => {
        // The token 
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers:{
                Authorization:`Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if(email !== "" && password !== ""){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email ,password])

    return(

        (user.id !== null)
        ?
            <Navigate to="/products/menu" />

        :    

        <>
            <h1 className = "my-5 text-center">Login</h1>
            <Form onSubmit = {(e) => loginUser(e)}>
                <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} />
                </Form.Group>
        
                <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} />
                </Form.Group>

                {
                    isActive
                    ?
                        <Button variant="primary" type="submit" id="submitBtn">
                        Login
                        </Button>
                    :
                        <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Login
                    </Button>
                }
        </Form>
      </>
    )
}