import Banner from "../components/Banner";


export default function Home(){
    const data = {
        title: "Just Cakes",
        content: "Bringing delicious cakes since 2022",
        destination: "/products/menu",
        label: "Menu"
    }

    return(
        <>
            <Banner data={data} />
        </>
    )
}