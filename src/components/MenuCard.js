import {Link} from "react-router-dom"

import {Row, Col, Card, Button} from "react-bootstrap";

export default function MenuCard({menuProp}){

    // Deconstruct properties
    const {_id, name, description, price, stocks} = menuProp;

    return (
        <Row className="mt-3 mb-3">
            <Col>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            {name}
                        </Card.Title>
                        <Card.Subtitle>
                            Description:
                        </Card.Subtitle>
                        <Card.Text>
                            {description}
                        </Card.Text>
                        <Card.Subtitle>
                            Price:
                        </Card.Subtitle>
                        <Card.Text>
                            {price}
                        </Card.Text>
                        <Card.Text>
                            Stocks : {stocks}
                        </Card.Text>
                        <Button as={Link} to={`/products/${_id}`} variant = "primary">Details</Button>
                        
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}